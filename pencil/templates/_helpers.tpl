{{- define "pencil.name" -}}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- end }}

{{- define "pencil.labels" -}}
app: "{{ .Chart.Name }}"
environment: "{{ .Release.Name }}"
{{- end }}

{{- define "pencil.selectorLabels" -}}
app: "service"
{{ .Release.Name }}-target: "true"
{{- end }}

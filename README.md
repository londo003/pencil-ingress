# pencil-ingress

use pod labels to dynamically switch the service attached to an ingress to different pods

## deploy services

deploy a review and a development service 'environment'
```bash
helm install review-service service --atomic
helm install development-service service --atomic
```

deploy a review and development pencil environment

```bash
helm install development-pencil pencil
helm install review-pencil pencil
```
if deploying to normal k8s with ingress instead of routes, leave off , but set pencilUrl to a url that works in your network
```bash
helm install development-pencil pencil --set-string pencilUrl=$aurl
helm install review-pencil pencil --set-string pencilUrl=$anotherurl
```

you now have 2 'service_environmets'
- development-service
- review-service

and 2 'pencil_environments
- development-pencil
- review-pencil

If you curl the url for either pencil you will either get 404, or an Openshift error page saying the routes exist but no pods are available

openshift:
```bash
curl https://development-pencil.ocp.dhe.duke.edu
curl https://review-pencil.ocp.dhe.duke.edu
```

other k8s
```bash
curl http://$aurl
curl http://$anotherurl
```

Use the ./choose.sh command to target a pencil_environment to a specific service_environment
```bash
./choose.sh development-pencil review-service
curl https://development-pencil.ocp.dhe.duke.edu | grep Hostname 
./choose.sh development-pencil development-service
./choose.sh review-pencil review-service
curl https://development-pencil.ocp.dhe.duke.edu | grep Hostname
curl https://review-pencil.ocp.dhe.duke.edu | grep Hostname
```

It is also possible for a service to have 2 targets
```bash
./choose.sh development-pencil development-service
./choose.sh development-pencil review-service
curl https://development-pencil.ocp.dhe.duke.edu | grep Hostname
curl https://review-pencil.ocp.dhe.duke.edu | grep Hostname
```

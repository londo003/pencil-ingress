pencil_environment=$1
if [ -z "${pencil_environment}" ]
then
  echo "usage: ${0} [pencil_environment] [service_environment" >&2
  exit
fi

service_environment=$2
if [ -z "${service_environment}" ]
then
  echo "usage: ${0} [pencil_environment] [service_environment" >&2
  exit
fi

if ! kubectl get service -l "app=pencil,environment=${pencil_environment}" -o name | grep ${pencil_environment}
then
  echo "pencil service ${pencil_environment} not present" >&2
  exit 1
fi

if ! kubectl get pods -l "app=service,environment=${service_environment}" -o name | grep service
then
  echo "service ${service_environment} pods not present" >&2
  exit 1
fi

kubectl label pods -l "app=service,${pencil_environment}-target=true" ${pencil_environment}-target-
kubectl label pods -l "app=service,environment=${service_environment}" ${pencil_environment}-target=true


{{- define "service.name" -}}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- end }}

{{- define "service.labels" -}}
app: "{{ .Chart.Name }}"
environment: "{{ .Release.Name }}"
{{- end }}
